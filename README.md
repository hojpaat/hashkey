
# Hash key validation

## Description of the program

This RESTful WebApp able to validate a hash (MD5 or SHA-1) generated from a string.

## Run the app locally

#### Clone this repository

> git clone https://gitlab.com/hojpaat/hashkey.git

#### Dependencies
 - org.springframework.boot: spring-boot-starter
 - org.springframework.boot: spring-boot-starter-web
 - org.projectlombok: lombok (make sure the annotation processing is enabled)

#### Environment variables

| Key        | Value           |
| ------------- |:-------------:|
| PORT_NUMBER | chosen port number (default: 8080)  |

### Start the application
The launcher class is HashkeyApplication, after successful launching it can access access the application in localhost:PORT_NUMBER

## API spec

### GET /hashkeyValidation/MD5

Validate a MD5 Hashkey of a given String

#### Request parameters

```json
{ 
	"inputString": "Test case",
	"hashkey": "c639efc1e98762233743a75e7798dd9c"
}
```

#### Response

 - if the MD5 hashkey equal to the hashkey generated from the input string: HTTP Status 200

```json
{
	"result": "The given MD5 hashkey is valid"
}
```

 - if the MD5 hashkey is different than the hashkey generated from the input string: HTTP Status 418

```json
{
	"result": "The given MD5 hashkey is different"
}
```

### GET /hashkeyValidation/SHA1

Validate a SHA-1 Hashkey of a given String

#### Request parameters

```json
{ 
	"inputString": "Test case",
	"hashkey": "c639efc1e98762233743a75e7798dd9c"
}
```

#### Response

 - if the SHA-1 hashkey equal to the hashkey generated from the input string: HTTP Status 200

```json
{
	"result": "The given hashkey is valid"
}
```

 - if the SHA-1 hashkey is different than the hashkey generated from the input string: HTTP Status 418

```json

{
	"result": "The given hashkey is different"
}
```