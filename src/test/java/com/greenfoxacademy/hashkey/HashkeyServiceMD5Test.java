package com.greenfoxacademy.hashkey;

import org.junit.Before;
import org.junit.Test;

import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.*;

public class HashkeyServiceMD5Test {
  
  private HashkeyServiceMD5 hashkeyServiceMD5;
  private String testString;
  private String testStringHashkey;
  private String lowercaseHashkey;

  @Before
  public void init(){
    hashkeyServiceMD5 = new HashkeyServiceMD5();
    testString = "This is a test string";
    testStringHashkey = "C639EFC1E98762233743A75E7798DD9C";
    lowercaseHashkey = "c639efc1e98762233743a75e7798dd9c";
  }
  
  @Test
  public void createHashkey_succesfully() throws NoSuchAlgorithmException {
    assertEquals(testStringHashkey, hashkeyServiceMD5.createHashkey(testString));
    assertFalse("".equals(hashkeyServiceMD5.createHashkey(testString)));
    assertEquals(lowercaseHashkey.toUpperCase(),
            hashkeyServiceMD5.createHashkey(testString));
    assertTrue(hashkeyServiceMD5.createHashkey(testString) instanceof String);
  }
  
  @Test
  public void validateHashkey_validHashkey_returnTrue() throws NoSuchAlgorithmException {
    assertTrue(hashkeyServiceMD5.validateHashkey(testString, testStringHashkey));
    assertTrue(hashkeyServiceMD5.validateHashkey(testString, lowercaseHashkey));
  }
  
  @Test
  public void validateHashkey_validHashkey_returnFalse() throws NoSuchAlgorithmException {
    
    assertFalse(hashkeyServiceMD5.validateHashkey(testString,
            hashkeyServiceMD5.createHashkey("this is a test string")));
    assertFalse(hashkeyServiceMD5.validateHashkey(testString, ""));
    assertFalse(hashkeyServiceMD5.validateHashkey(testString, "This is a test string"));
  }
}
