package com.greenfoxacademy.hashkey;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.*;

public class HashkeyServiceSHA1Test {
  
  private HashkeyServiceSHA1 hashkeyServiceSHA1;
  private String testString;
  private String testStringHashkey;
  private String lowercaseHashkey;
  
  @Before
  public void init(){
    hashkeyServiceSHA1 = new HashkeyServiceSHA1();
    testString = "This is a test string";
    testStringHashkey = "E2F67C772368ACDEEE6A2242C535C6CC28D8E0ED";
    lowercaseHashkey = "e2f67c772368acdeee6a2242c535c6cc28d8e0ed";
  }
  
  @Test
  public void createHashkey_succesfully() throws NoSuchAlgorithmException {
    Assert.assertEquals(testStringHashkey, hashkeyServiceSHA1.createHashkey(testString));
    assertFalse("".equals(hashkeyServiceSHA1.createHashkey(testString)));
    Assert.assertEquals(lowercaseHashkey.toUpperCase(),
            hashkeyServiceSHA1.createHashkey(testString));
    assertTrue(hashkeyServiceSHA1.createHashkey(testString) instanceof String);
  }
  
  @Test
  public void validateHashkey_validHashkey_returnTrue() throws NoSuchAlgorithmException {
    assertTrue(hashkeyServiceSHA1.validateHashkey(testString, testStringHashkey));
    assertTrue(hashkeyServiceSHA1.validateHashkey(testString, lowercaseHashkey));
  }
  
  @Test
  public void validateHashkey_validHashkey_returnFalse() throws NoSuchAlgorithmException{
    assertFalse(hashkeyServiceSHA1.validateHashkey(testString,
            hashkeyServiceSHA1.createHashkey("this is a test string")));
    assertFalse(hashkeyServiceSHA1.validateHashkey(testString, ""));
    assertFalse((hashkeyServiceSHA1.validateHashkey(testString, "This is a test string")));
  }
}
