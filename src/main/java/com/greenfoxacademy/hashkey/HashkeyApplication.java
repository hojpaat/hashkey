package com.greenfoxacademy.hashkey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HashkeyApplication {
  
  public static void main(String[] args) {
    SpringApplication.run(HashkeyApplication.class, args);
  }
  
}
