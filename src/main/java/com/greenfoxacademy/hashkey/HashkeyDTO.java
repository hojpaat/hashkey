package com.greenfoxacademy.hashkey;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HashkeyDTO {
  private String hashkey;
  private String inputString;
}
