package com.greenfoxacademy.hashkey;

import org.springframework.http.ResponseEntity;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public interface HashkeyService {
  
  String createHashkey(String input) throws NoSuchAlgorithmException;
  
  boolean validateHashkey(String inputData, String hashkey) throws NoSuchAlgorithmException;
  
  ResponseEntity<HashMap<String, String>> validateHashkeyFromDTO(HashkeyDTO hashkeyDTO) throws NoSuchAlgorithmException;
}
