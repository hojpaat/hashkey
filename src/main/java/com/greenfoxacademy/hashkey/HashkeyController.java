package com.greenfoxacademy.hashkey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.NoSuchAlgorithmException;

@RestController
public class HashkeyController {
  
  @Autowired
  @Qualifier("MD5")
  HashkeyService hashkeyServiceMD5;
  
  @Autowired
  @Qualifier("SHA1")
   HashkeyService hashkeyServiceSHA1;
  
  
  @GetMapping("/hashkeyValidation/MD5")
  private ResponseEntity validateMD5Hashkey(@RequestBody HashkeyDTO hashkeyDTO) throws NoSuchAlgorithmException {
    return hashkeyServiceMD5.validateHashkeyFromDTO(hashkeyDTO);
  }
  
  @GetMapping("/hashkeyValidation/SHA1")
  private ResponseEntity validateSHA1Hashkey(@RequestBody HashkeyDTO hashkeyDTO) throws NoSuchAlgorithmException {
    return hashkeyServiceSHA1.validateHashkeyFromDTO(hashkeyDTO);
  }
}
