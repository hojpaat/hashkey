package com.greenfoxacademy.hashkey;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

@Service
@Qualifier("MD5")
public class HashkeyServiceMD5 implements HashkeyService {

  @Override
  public String createHashkey(String input) throws NoSuchAlgorithmException {
    MessageDigest md = MessageDigest.getInstance("MD5");
    md.update(input.getBytes());
    byte[] digest = md.digest();
    return DatatypeConverter
            .printHexBinary(digest).toUpperCase();
  }
  
  @Override
  public boolean validateHashkey(String inputData, String hashkey) throws NoSuchAlgorithmException {
    return hashkey.toUpperCase().equals(createHashkey(inputData));
  }
  
  public ResponseEntity<HashMap<String, String>> validateHashkeyFromDTO(HashkeyDTO hashkeyDTO) throws NoSuchAlgorithmException {
    HashMap<String, String> result = new HashMap<>();
    boolean resultOfValidation = validateHashkey(hashkeyDTO.getInputString(), hashkeyDTO.getHashkey());
    String resultOfHashkeyValidation;
    resultOfHashkeyValidation = resultOfValidation ? "The given MD5 hashkey is valid" : "The given MD5 hashkey is different";
    result.put("result", resultOfHashkeyValidation);
    int status;
    status = resultOfValidation ? 200 : 404;
    return ResponseEntity.status(status).body(result);
  }
}
